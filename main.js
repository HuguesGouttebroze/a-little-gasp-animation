import './animation.css'
import javascriptLogo from './javascript.svg'
import { setupCounter } from './counter.js'
import { TimelineLite } from 'gsap/gsap-core';
import { TweenLite } from 'gsap/gsap-core';
// import { gsap } from "gsap";
import { gsap } from 'gsap-trial';
import { MotionPathPlugin } from 'gsap-trial/MotionPathPlugin';

gsap.registerPlugin(TimelineLite, TweenLite, MotionPathPlugin);

gsap.to("#rect", {
  duration: 5,
  repeat: 12,
  repeatDelay: 3,
  yoyo: true,
  ease: "power1.inOut",
  motionPath: {
    path: "#path",
    align:"#path",
    autoRotate: true,
    alignOrigin: [0.5, 0.5]
  }
})
/**
 * on bezier plugin
 */
/* const flightPath = {
  curviness: 1.25,
  autoRotate: true,
  values: [
    {
      x: 100,
      y: -20
    }
  ],
};
 */
/* const tween = new TimelineLite(); */

/**
 * TweenlineLite()
 * first params define the object to animated, here ".picture"
 * second params define the direction
 * third define property we wanna animated
 */
/* tween.add(
  TweenLite.to(".picture", 1, {
    motionPath: flightPath,
    ease: Power1.easeInOut
  })
) */

setupCounter(document.querySelector('#counter'))
