import React from 'react'

export default function Dragable() {
  return (
    <>
        <div class="hub__wrapper">
	        <div id="miniMap" class="position-fixed mini-map">
	        	<div id="mapMarker" class="position-absolute mini-map-marker"></div>
	        </div>
            <div class="hub__container">
                <div class="hub hub-day">
                    <div class="hub__inner-wrapper">
                        <img id="light__off" src="https://jkrstories.clientapproval.co.uk/wp-content/themes/ed-theme/assets/img/JKR_Childrens_HUB_DAY_LIGHT_OFF.jpg" />		
                    </div>
                </div>
            </div>
        </div>
    </>
  )
}
